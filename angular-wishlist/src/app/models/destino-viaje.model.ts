import {v4 as uuid} from 'uuid';
import { VoteUpAction } from './destinos-viajes-state.model';

export class DestinoViaje {
  id = uuid();
  selected: boolean;
  servicios: string[];

    constructor(public nombre: string, public imageUrl:string, public votes: number = 0) {
        this.servicios = ['pileta', 'desayuno'];
      }
      
      setSelected(s:boolean){
        this.selected = s;
      };
      isSelected() {
        return this.selected;
      };
      voteUp(): any {
        this.votes++;
      };
      voteDown(): any {
        this.votes--;
      };
}
