import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  authService: any;
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
  mensajeError: string;

  consructor(/*public authService: AuthService*/) {
    this.mensajeError = '';
  }

  ngOnInIt() {

  }

  login(username: string, password: string): boolean {
    this.mensajeError = '';
    if (!this.authService.login(username, password)) {
      this.mensajeError = 'Login incorrecto.';
      setTimeout(function() {
        this.mensajeError = '';
      }.bind(this), 2500);
    }
    return false;
  }

  logout(): boolean {
    return this.authService.logout();
  }

}
